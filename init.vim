execute pathogen#infect()
let g:AutoPairsMultilineClose = 0
call deoplete#enable()
let g:deoplete#enable_at_startup = 1

autocmd FileType javascript setlocal equalprg=
    \js-beautify\ --config\ ~/.jsbeautifyrc\ -f\ -

syntax on
filetype plugin on
colorscheme peachpuff

set expandtab
set modeline
set softtabstop=0
set shiftwidth=4
set tabstop=4

set hidden
set number

set list
set listchars=tab:\ \ ,nbsp:·,trail:·

highlight Visual ctermbg=235 ctermfg=None

set foldmethod=syntax
set foldlevelstart=10
set foldlevel=10

nnoremap <silent> <Space> za
nnoremap <silent> <C-Space> zA

nnoremap <C-A-Up> 		:m-2<CR>==
nnoremap <C-A-Down> 	:m+<CR>==
inoremap <C-A-Up> 		<Esc>:m-2<CR>==gi
inoremap <C-A-Down> 	<Esc>:m+<CR>==gi

"nnoremap <Up> 			gk
"nnoremap <Down> 		gj
"inoremap <Up> 			<Esc>gka
"inoremap <Down> 		<Esc>gja

nmap     <Tab>          gt
nmap     <S-Tab>        gT

nnoremap <C-S-d>	 	mzyyp`z
inoremap <C-d> 			<Esc>yypa

function! NetrwOpenMultiTab(current_line,...) range
	" Get the number of lines.
	let n_lines =	a:lastline - a:firstline + 1

	" This is the command to be built up.
	let command = "normal "

	" Iterator.
	let i = 1

	" Virtually iterate over each line and build the command.
	while i < n_lines
		let command .= "tgT:" . ( a:firstline + i ) . "\<CR>:+tabmove\<CR>"
		let i += 1
	endwhile
	let command .= "tgT"

	" Restore the Explore tab position.
	if i != 1
		let command .= ":tabmove -" . ( n_lines - 1 ) . "\<CR>"
	endif

	" Restore the previous cursor line.
	let command .= ":" . a:current_line  . "\<CR>"

	" Check function arguments
	if a:0 > 0
		if a:1 > 0 && a:1 <= n_lines
			" The current tab is for the nth file.
			let command .= ( tabpagenr() + a:1 ) . "gt"
		else
			" The current tab is for the last selected file.
			let command .= (tabpagenr() + n_lines) . "gt"
		endif
	endif
	" The current tab is for the Explore tab by default.

	" Execute the custom command.
	execute command
endfunction

" Define mappings.
augroup NetrwOpenMultiTabGroup
	autocmd!
	autocmd Filetype netrw vnoremap <buffer> <silent> <expr> t ":call NetrwOpenMultiTab(" . line(".") . "," . "v:count)\<CR>"
	autocmd Filetype netrw vnoremap <buffer> <silent> <expr> T ":call NetrwOpenMultiTab(" . line(".") . "," . (( v:count == 0) ? '' : v:count) . ")\<CR>"
augroup END

highlight OverLength ctermbg=52 ctermfg=white
match OverLength /\%81v.\+/
